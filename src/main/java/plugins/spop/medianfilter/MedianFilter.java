package plugins.spop.medianfilter;

import java.util.Arrays;

import icy.image.IcyBufferedImage;
import icy.math.ArrayMath;
import icy.sequence.Sequence;
import icy.system.IcyHandledException;
import icy.type.collection.array.Array1DUtil;
import icy.util.OMEUtil;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzStoppable;
import plugins.adufour.ezplug.EzVarInteger;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.ezplug.EzVarText;

public class MedianFilter extends EzPlug implements Block, EzStoppable
{

    /**
     * Median Filter
     * 
     * @author Sorin POP
     */

    EzVarSequence input = new EzVarSequence("Input");
    EzVarText type_2D = new EzVarText("Type", new String[] {"2D", "3D"}, 0, false);
    EzVarInteger size = new EzVarInteger("Half Size", 2, 1, 30, 1);

    EzVarSequence output = new EzVarSequence("Output");

    @Override
    public void initialize()
    {
        super.addEzComponent(input);
        super.addEzComponent(type_2D);
        super.addEzComponent(size);
        super.setTimeDisplay(true);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add(input.name, input.getVariable());
        inputMap.add(type_2D.name, type_2D.getVariable());
        inputMap.add(size.name, size.getVariable());
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add(output.name, output.getVariable());
    }

    @Override
    public void execute()
    {
        Sequence sequence = input.getValue(true);

        int dim_window = size.getValue();
        try
        {
            if (type_2D.getValue().equalsIgnoreCase("2D"))
                Median_filter_2D(sequence, dim_window);
            else
                Median_filter_3D(sequence, dim_window);
        }
        catch (Exception e)
        {
            throw new IcyHandledException(e);
        }
    }

    public void Median_filter_2D(Sequence sequence, int lx) throws InterruptedException
    {
        int no_elem, temp, i;

        no_elem = (2 * lx + 1) * (2 * lx + 1);
        double[] tab_elem = new double[no_elem];

        Sequence seq_out = new Sequence(OMEUtil.createOMEXMLMetadata(sequence.getOMEXMLMetadata()));
        int dim_x = sequence.getSizeX();
        int dim_y = sequence.getSizeY();
        int dim_z = sequence.getSizeZ();
        int dim_t = sequence.getSizeT();
        int dim_c = sequence.getSizeC();

        seq_out.beginUpdate();
        for (int time_slice = 0; time_slice < dim_t; time_slice++)
        {
            for (int z = 0; z < dim_z; z++)
            {
                IcyBufferedImage icy_img = new IcyBufferedImage(dim_x, dim_y, sequence.getColorModel());
                for (int c = 0; c < dim_c; c++)
                {
                    if (Thread.interrupted())
                        throw new InterruptedException();

                    Object sequenceDataXY = sequence.getDataXY(time_slice, z, c);
                    double[] tab = Array1DUtil.arrayToDoubleArray(sequenceDataXY, false);
                    double[] tab_out = Arrays.copyOf(tab, tab.length);

                    for (int x = lx; x < dim_x - lx; x++)
                    {
                        for (int y = lx; y < dim_y - lx; y++)
                        {
                            i = 0;
                            temp = y * dim_x + x;
                            for (int xl = -lx; xl <= lx; xl++)
                                for (int yl = -lx; yl <= lx; yl++)
                                {
                                    tab_elem[i] = tab[temp + yl * dim_x + xl];
                                    i++;
                                }
                            tab_out[temp] = ArrayMath.median(tab_elem, false);
                        }
                    }
                    Array1DUtil.doubleArrayToSafeArray(tab_out, icy_img.getDataXY(c), icy_img.isSignedDataType());
                }
                seq_out.setImage(time_slice, z, icy_img);

            }
        }
        seq_out.endUpdate();

        for (int c = 0; c < sequence.getSizeC(); c++)
        {
            seq_out.setChannelName(c, sequence.getChannelName(c));
            seq_out.setDefaultColormap(c, sequence.getDefaultColorMap(c), true);
            seq_out.setColormap(c, sequence.getColorMap(c));
        }

        seq_out.setName(seq_out.getName() + "_Median2D(" + lx + ")");
        if (getUI() != null)
            addSequence(seq_out);
        output.setValue(seq_out);

    }

    public void Median_filter_3D(Sequence sequence, int lx) throws InterruptedException
    {
        int no_elem, temp, i;

        no_elem = (2 * lx + 1) * (2 * lx + 1) * (2 * lx + 1);
        double[] tab_elem = new double[no_elem];
        double[] tab_elem_2D = new double[(2 * lx + 1) * (2 * lx + 1)];

        Sequence seq_out = new Sequence(OMEUtil.createOMEXMLMetadata(sequence.getOMEXMLMetadata()));
        int dim_x = sequence.getSizeX();
        int dim_y = sequence.getSizeY();
        int dim_z = sequence.getSizeZ();
        int dim_t = sequence.getSizeT();
        int dim_c = sequence.getSizeC();

        double[][] tab = new double[dim_z][];
        double[][] tab_out = new double[dim_z][dim_x * dim_y];

        seq_out.beginUpdate();
        for (int time_slice = 0; time_slice < dim_t; time_slice++)
        {
            IcyBufferedImage[] icy_img = new IcyBufferedImage[dim_z];
            for (int z = 0; z < dim_z; z++)
                icy_img[z] = new IcyBufferedImage(dim_x, dim_y, sequence.getColorModel());

            for (int c = 0; c < dim_c; c++)
            {
                for (int z = 0; z < dim_z; z++)
                {
                    tab[z] = Array1DUtil.arrayToDoubleArray(sequence.getDataXY(time_slice, z, c), false);
                    tab_out[z] = Arrays.copyOf(tab[z], tab[z].length);
                }

                for (int z = 0; z < dim_z; z++)
                {
                    if (Thread.interrupted())
                        throw new InterruptedException();

                    if (z < lx || z >= dim_z - lx)
                    {
                        for (int x = lx; x < dim_x - lx; x++)
                        {
                            for (int y = lx; y < dim_y - lx; y++)
                            {
                                i = 0;
                                temp = y * dim_x + x;

                                for (int xl = -lx; xl <= lx; xl++)
                                    for (int yl = -lx; yl <= lx; yl++)
                                    {
                                        tab_elem_2D[i] = tab[z][temp + yl * dim_x + xl];
                                        i++;
                                    }
                                tab_out[z][temp] = ArrayMath.median(tab_elem_2D, false);
                            }
                        }
                    }
                    else
                    {
                        for (int x = lx; x < dim_x - lx; x++)
                            for (int y = lx; y < dim_y - lx; y++)
                            {
                                i = 0;
                                temp = y * dim_x + x;
                                for (int zl = -lx; zl <= lx; zl++)
                                    for (int xl = -lx; xl <= lx; xl++)
                                        for (int yl = -lx; yl <= lx; yl++)
                                        {
                                            tab_elem[i] = tab[z + zl][temp + yl * dim_x + xl];
                                            i++;
                                        }
                                tab_out[z][temp] = ArrayMath.median(tab_elem, false);
                            }
                    }
                }

                for (int z = 0; z < dim_z; z++)
                    Array1DUtil.doubleArrayToSafeArray(tab_out[z], icy_img[z].getDataXY(c),
                            icy_img[z].isSignedDataType());
                System.gc();
            }
            for (int z = 0; z < dim_z; z++)
                seq_out.setImage(time_slice, z, icy_img[z]);

        }
        seq_out.endUpdate();

        for (int c = 0; c < sequence.getSizeC(); c++)
        {
            seq_out.setChannelName(c, sequence.getChannelName(c));
            seq_out.setDefaultColormap(c, sequence.getDefaultColorMap(c), true);
            seq_out.setColormap(c, sequence.getColorMap(c));
        }

        seq_out.setName(sequence.getName() + "_Median3D(" + lx + ")");
        if (getUI() != null)
            addSequence(seq_out);
        output.setValue(seq_out);
    }

    public void clean()
    {
    }

}
